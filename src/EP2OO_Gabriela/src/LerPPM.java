package EP2OO_Gabriela.src;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;



public class LerPPM extends Leitura {
	
	private int altura ;
	private int largura ;
	private int maxVal  ;
	
	public void setNumeroMagico(){
		numeroMagico = "P6";
	}
	
	 public static String le_linha(FileInputStream arquivo) {
			String linha = "";
			byte bb;
			try {
				while ((bb = (byte) arquivo.read()) != '\n') {
					linha += (char) bb;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Linha: " + linha);
			return linha;
		}

	 public void leitura (String linha, String numeroMagico, FileInputStream arquivo, int maxVal, int altura, int largura, int count){	
		
			linha = LerPPM.le_linha(arquivo);
			while (linha.startsWith("#")) {
				linha = LerPPM.le_linha(arquivo);
			}
		    Scanner in = new Scanner(linha); 
		    if(in.hasNext() && in.hasNextInt())
		    	this.largura = largura = in.nextInt();
		    else
		    	System.out.println("Arquivo corrompido");
		    if(in.hasNext() && in.hasNextInt())
		    	this.altura = altura = in.nextInt();
		    else
		    	System.out.println("Arquivo corrompido");
			linha = LerPPM.le_linha(arquivo);
			in.close();
            in = new Scanner(linha);
			this.maxVal = maxVal = in.nextInt();
			in.close();
			
			
			JOptionPane.showMessageDialog(null, "Altura=" + altura + "\n" + "Largura=" + largura
					+ "\n" + "Total de Pixels = " + (largura * altura) + "\n" + "Total de Pixels lidos = " + count );
			
			
	}

	public int getAltura() {
		return altura;
	}


	public int getLargura() {
		return largura;
	}


	public int getMaxVal() {
		return maxVal;
	}

	
}