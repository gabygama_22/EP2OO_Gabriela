package EP2OO_Gabriela.src;
import java.io.File;

 class MyCustomFilter extends javax.swing.filechooser.FileFilter {
        @Override
        public boolean accept(File file) {
           
            return file.isDirectory() || file.getAbsolutePath().endsWith(".pgm") || file.getAbsolutePath().endsWith(".ppm");
        }
        @Override
        public String getDescription() {
        	
            return "Image files (*.pgm,*.ppm)";
        }
    } 